#!/bin/bash
benchmark=$1
echo $benchmark
shift
if [ $benchmark == gcc ]; then
	echo "executing gcc with sim-bpred $@"
	cd ./benchmarks && ./sim-bpred $@ cc1.alpha -O 1stmt.i
elif [ $benchmark == go ]; then
	echo "executing go with sim-bpred $@"
	cd ./benchmarks && ./sim-bpred $@ go.alpha 50 9 2stone9.in 
elif [ $benchmark == ana ]; then
	echo "executing anagram with sim-bpred $@"
	cd ./benchmarks && ./sim-bpred $@ anagram.alpha words < anagram.in 
fi


