#!/bin/bash


# Running unmodified sim-bpred 
# bzip2
./raw-sim-bpred -max:inst 1500000000 ./bzip2/bzip2.base.alpha ./bzip2/ref/input/chicken.jpg 30 2> bzip2_chicken_unmodified.err &
./raw-sim-bpred -max:inst 1500000000 ./bzip2/bzip2.base.alpha ./bzip2/ref/input/input.source 280 2> bzip2_source_unmodified.err &
./raw-sim-bpred -max:inst 1500000000 ./bzip2/bzip2.base.alpha ./bzip2/ref/input/liberty.jpg 30 2> bzip2_liberty_unmodified.err &
./raw-sim-bpred -max:inst 1500000000 ./bzip2/bzip2.base.alpha ./bzip2/ref/input/text.html 280 2> bzip2_text_unmodified.err &

# hmmer
./raw-sim-bpred -max:inst 1500000000 ./hmmer/hmmer.base.alpha ./hmmer/ref/input/nph3.hmm ./hmmer/ref/input/swiss41 2> hmmer_nph3_unmodified.err &
./raw-sim-bpred -max:inst 1500000000 ./hmmer/hmmer.base.alpha --fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 ./hmmer/ref/input/retro.hmm ./hmmer/ref/input/swiss41 2> hmmer_retro_unmodified.err &

# mcf
./raw-sim-bpred -max:inst 1500000000 ./mcf/mcf.base.alpha ./mcf/ref/input/inp.in 2> mcf_inp_unmodified.err &

# sjeng
./raw-sim-bpred -max:inst 1500000000 ./sjeng/sjeng.base.alpha ./sjeng/ref/input/ref.txt 2> sjeng_ref_unmodified.err &


for size in 32 64 128 256 512
do
    for assoc in 1 2 4 8 16 32
        do
            sets=$((size/assoc))
            # bzip2
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./bzip2/bzip2.base.alpha ./bzip2/ref/input/chicken.jpg 30 2> bzip2_chicken_${sets}_${assoc}.err &
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./bzip2/bzip2.base.alpha ./bzip2/ref/input/input.source 280 2> bzip2_source_${sets}_${assoc}.err &
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./bzip2/bzip2.base.alpha ./bzip2/ref/input/liberty.jpg 30 2> bzip2_liberty_${sets}_${assoc}.err &
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./bzip2/bzip2.base.alpha ./bzip2/ref/input/text.html 280 2> bzip2_text_${sets}_${assoc}.err &

            # hmmer
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./hmmer/hmmer.base.alpha ./hmmer/ref/input/nph3.hmm ./hmmer/ref/input/swiss41 2> hmmer_nph3_${sets}_${assoc}.err &
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./hmmer/hmmer.base.alpha --fixed 0 --mean 500 --num 500000 --sd 350 --seed 0 ./hmmer/ref/input/retro.hmm ./hmmer/ref/input/swiss41 2> hmmer_retro_${sets}_${assoc}.err &

            # mcf
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./mcf/mcf.base.alpha ./mcf/ref/input/inp.in 2> mcf_inp_${sets}_${assoc}.err &

            # sjeng
            ./sim-bpred-ltb -max:inst 1500000000 -ltpred:ltb $sets $assoc ./sjeng/sjeng.base.alpha ./sjeng/ref/input/ref.txt 2> sjeng_ref_${sets}_${assoc}.err &
            sleep 60
        done
done
